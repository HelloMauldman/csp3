import React, { useContext, useState } from 'react';
import { Navbar, Nav, Container, NavDropdown, Fade, Badge } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';

const AppNavBar = ()=>{
	const {user, cartBadge} = useContext(UserContext)
    const[open, setOpen] = useState(false);


	return(
		// <Navbar bg="secondary" variant="light" expand="lg">
  //          <Link className="navbar-brand" to="/">The Zuitt Shop</Link>
  //          <Navbar.Toggle aria-controls="basic-navbar-nav" />
  //          <Navbar.Collapse id="basic-navbar-nav">
  //              <Nav className="mr-auto">
  //                  <Link className="nav-link" to="/products">
  //                      {user.isAdmin === true ?
  //                              <span>Admin Dashboard</span>
  //                          :
  //                              <span>Products</span>
  //                      }  
  //                  </Link>
  //              </Nav>
  //              <Nav className="ml-auto">
  //                  {user.id !== null ?
  //                          user.isAdmin === true ?
  //                                  <Link className="nav-link" to="/logout">
  //                                      Log Out, {user.email}
  //                                  </Link>
  //                              :
  //                                  <React.Fragment>
  //                                      <Link className="nav-link" to="/cart">
  //                                          Cart
  //                                      </Link>
  //                                      <Link className="nav-link" to="/orders">
  //                                          Orders
  //                                      </Link>
  //                                      <Link className="nav-link" to="/logout">
  //                                          Log Out, {user.email}
  //                                      </Link>
  //                                  </React.Fragment>
  //                      :
  //                          <React.Fragment>
  //                              <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>
  //                                  Log In
  //                              </Link>
  //                              <Link className="nav-link" to="/register">
  //                                  Register
  //                              </Link>
  //                          </React.Fragment>
  //                  }              
  //              </Nav>
  //          </Navbar.Collapse>
  //      </Navbar>
            <Navbar bg="dark" variant="dark" expand="lg" sticky="top" className="navbar">
                <Container>
                    <Navbar.Brand href="/">SampleShop</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Link className="nav-link" to="/products">
                                { user.isAdmin === true ? <span>Admin Dashboard</span> : <span>Products</span> }   
                            </Link>
                        </Nav>
                        <Nav className="mr-auto">
                            {user.id !== null 
                                ? 
                                user.isAdmin === true ? <Link className="nav-link" to="/logout">Log Out</Link> :
                                    <React.Fragment> 
                                        <Link className="nav-link" to="/cart">
                                            Cart<Badge pill bg="danger" className="position-absolute translate-middle">{cartBadge <= 0 || cartBadge === null ? 
                                                    ""
                                                :
                                                    cartBadge.length
                                            }</Badge>
                                        </Link>
                                        
                                        <Link className="nav-link" to="/orders">Orders</Link>
                                        <NavDropdown title={user.email} id="basic-nav-dropdown" onClick={()=>{setOpen(!open)}} aria-expanded={open}>
                                            <Fade in={open}>
                                                <NavDropdown.Item>
                                                    <Link className="nav-link text-dark" to="/logout">Log Out</Link>
                                                </NavDropdown.Item> 
                                            </Fade>
                                        </NavDropdown>
                                    </React.Fragment>
                                :
                                <React.Fragment>
                                    <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>Log In</Link>
                                    <Link className="nav-link" to="/register">Register</Link>
                                </React.Fragment>
                            } 
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

	);
}

export default AppNavBar;