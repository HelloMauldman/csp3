import React from 'react';
import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Product = (props)=>{
	const { data} = props;
	const {_id, name, description, price} = data;

	return(
       <Col xs={12} md={3} className="mt-4">
           <Card className="card1 m-3 rounded-3">
               <Card.Body>
                   <Card.Title className="text-center card2">
                       <Link to={`/products/${_id}`}>
                           {name}
                       </Link>
                   </Card.Title>
                   <Card.Text className="card3">{description}</Card.Text>
                   <h5 className="text-warning">₱{price}</h5>
               </Card.Body>
               <Card.Footer className="d-grid">
                   <Link
                       className="btn btn-primary btn-block rounded-3"
                       to={`/products/${_id}`}
                   >
                       Details
                   </Link>
               </Card.Footer>
           </Card>
       </Col>
   );
}

export default Product;