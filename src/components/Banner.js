import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom'


const Banner = ({ data })=>{
	const {title, content, destination, label} = data;
	return(
		<Row className="py-5 m-5 bLight bg-gradient rounded-3">
			<Col>
				<div className="text-center display-5 m-5 px-5">
					<h1 className="fs-1 fw-bold">{title}</h1>
					<p id="motto" className="md-8 fs-4">{content}</p>
					<Link className="btn btn-primary rounded-3" to={destination}>
						{label}
					</Link>
				</div>
			</Col>
		</Row>
	);
}

export default Banner;