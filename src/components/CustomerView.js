import React, { useEffect, useState } from 'react';
import Product from '../components/Product';
import { Row } from 'react-bootstrap';
 
const CustomerView = ({productsData}) => {
   const [products, setProducts] = useState([]);
   useEffect(() => {
 
       const productsArr = productsData.map(productData => {
           if (productData.isActive === true) {
               return (
                   <Product data={productData} key={productData._id} breakpoint={4}/>
               );
           } else {
               return null;
           }
       });
 
       setProducts(productsArr);
 
   }, [productsData])
 
   return(
       <React.Fragment>
           <h2 className="text-center my-5">Our Products</h2>
           <Row>
               {products}
           </Row>
       </React.Fragment>
   );
  
}
export default CustomerView;
