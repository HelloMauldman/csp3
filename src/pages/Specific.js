import { useState, useEffect, useContext } from 'react';
import { Card, Container, Button, InputGroup, FormControl } from 'react-bootstrap';
import { Link, useParams } from 'react-router-dom';
import UserContext from '../UserContext';

const Specific = () => {

	const { user, checkCartNo } = useContext(UserContext);
	const { productId } = useParams();

	const [id, setId] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [qty, setQty] = useState(0);
	const [price, setPrice] = useState(0);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

			setId(data._id);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

	},[productId]);

	const reduceQty = () => {
		if (qty <= 1) {
			alert("Quantity can't be lower than 1.");
		} else {
			setQty(qty - 1);
		}
	};

	const addToCart = () => {

		let alreadyInCart = false;
		let productIndex;
		let message;
		let cart = [];

		if (localStorage.getItem('cart')) {
			cart = JSON.parse(localStorage.getItem('cart'));
		};

		for(let i = 0; i < cart.length; i++){
			if (cart[i].productId === id) {
				alreadyInCart = true;
				productIndex = i;
			}
		}

		if (alreadyInCart) {
			cart[productIndex].quantity += qty;
			cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity;
		} else {
			cart.push({
				'productId' : id,
				'name': name,
				'price': price,
				'quantity': qty,
				'subtotal': price * qty
			});		
		};

		localStorage.setItem('cart', JSON.stringify(cart));

		if (qty === 1) {
			message = "1 item added to cart.";
		} else {
			message = `${qty} items added to cart.`;
		}

		alert(message);
		checkCartNo(cart)

	}

	const qtyInput = (value) => {

		if (value === '') {
			value = 1;
		} else if (value === "0") {
			alert("Quantity can't be lower than 1.");
			value = 1;
		}

		setQty(value);

	}

	return (
		<Container>
			<Card className="mt-5 rounded-3">
				<Card.Header className="bg-secondary text-white text-center pb-0"><h4>{name}</h4></Card.Header>
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>
						Price: <span className="text-warning">₱{price}</span>
					</h6>
					<h6>Quantity:</h6>
					<InputGroup className="mt-3 mb-3 d-flex d-md-flex">
						{/* <InputGroup className="d-inline d-md-flex"> */}
						<Button bg="light" className="rounded-3"onClick={reduceQty} id="button-addon1">
							-
						</Button>
						{/* </InputGroup> */}
						<FormControl 
							type="number"
							min="1"
							value={qty}
							onChange={e => qtyInput(e.target.value)}
							className="inputAdjust"
						/>
						{/* <InputGroup className="d-none d-md-flex"> */}
						<Button
							bg="light"
							onClick={() => setQty(qty + 1)}
							className="rounded-3"
						>
							+
						</Button>
						{/* </InputGroup> */}
					</InputGroup>
				</Card.Body>
				<Card.Footer>
				{user.id !== null ? 
						user.isAdmin === true ?
								<Button variant="danger" block="true" className="rounded-3" disabled>Admin can't Add to Cart</Button>
							:
								// <Button variant="primary" block="true" className="rounded-3" onClick={addToCart} href="/products">Add to Cart</Button>
								<Link className="btn btn-primary btn-block rounded-3" to="/products" onClick={addToCart}>Add to Cart</Link>
					: 
						<Link className="btn btn-warning btn-block" to={{pathname: '/login', state: { from: 'cart'}}}>Log in to Add to Cart</Link>
				}
	      		</Card.Footer>
			</Card>
		</Container>
	)
}
 
export default Specific;