import React, { useState, useEffect, useContext } from 'react';
import { Container,  InputGroup, Button, FormControl, Table } from 'react-bootstrap'
import { Link, Navigate } from 'react-router-dom';
import UserContext from "../UserContext"

const MyCart = () => {
	const {checkCartNo} = useContext(UserContext);
	const [total, setTotal] = useState(0);
	const [cart, setCart] = useState([]);
	const [tableRows, setTableRows] = useState([]);
	const [willRedirect, setWillRedirect] = useState(false);
	useEffect(()=> {
		if (localStorage.getItem('cart')) {
			setCart(JSON.parse(localStorage.getItem('cart')));
		}
	},[])

	useEffect(()=> {

		const qtyInput = (productId, value) => {

			let tempCart = [...cart];

			if (value === '') {
				value = 1;
			} else if (value === "0") {
				alert("Quantity can't be lower than 1.");
				value = 1;
			}

			for(let i = 0; i < cart.length; i++){

				if(tempCart[i].productId === productId){
					tempCart[i].quantity = parseFloat(value);
					tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
				}

			}

			setCart(tempCart);
			localStorage.setItem('cart', JSON.stringify(tempCart));

		}

		const qtyBtns = (productId, operator) => {

			let tempCart = [...cart];

			for(let i = 0; i < tempCart.length; i++){

				if (tempCart[i].productId === productId) {

					if (operator === "+") {
						tempCart[i].quantity += 1;
						tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
					} else if (operator === "-") {
						if(tempCart[i].quantity <= 1){
							alert("Quantity can't be lower than 1.");
						}else{
							tempCart[i].quantity -= 1;
							tempCart[i].subtotal = tempCart[i].price * tempCart[i].quantity;
						}
					}

				}
			}

			setCart(tempCart);
			localStorage.setItem('cart', JSON.stringify(tempCart));

		}

		const removeBtn = (productId) => {

			let tempCart = [...cart];

			let cartIds = cart.map((item)=> {
				return item.productId;
			})

			// console.log(tempCart);

			tempCart.splice([cartIds.indexOf(productId)], 1);

			setCart(tempCart);
			localStorage.setItem('cart', JSON.stringify(tempCart));
			checkCartNo(cart);


		}

		let cartItems = cart.map((item, index) => {

			return (
		      <tr key={item.productId}>
		          <td>
		          	<Link to={`/products/${item.productId}`}>
		          		{item.name}
		          	</Link>
		          </td>
		          <td>₱{item.price}</td>
		          <td>
				  	<InputGroup className="d-md-none">
						<FormControl
							type="number"
							min="1"
							value={item.quantity}
							onChange={e => qtyInput(item.productId, e.target.value)}
						/>
					</InputGroup>
					<InputGroup className="d-none d-md-flex w-50">

						<InputGroup>
							<Button 
								variant="dark"
								onClick={() => qtyBtns(item.productId, "-")}
							>
								-
							</Button>
						</InputGroup>

						<FormControl
							type="number"
							min="1" value={item.quantity}
							onChange={e => qtyInput(item.productId, e.target.value)}
						/>

						<InputGroup>
							<Button 
								variant="dark"
								onClick={() => qtyBtns(item.productId, "+")}
							>
								+
							</Button>
						</InputGroup>

					</InputGroup>
		          </td>
		          <td>₱{item.subtotal}</td>
		          <td className="text-center">
		          	<Button 
		          		variant="danger"
		          		onClick={() => removeBtn(item.productId)}
		          	>
		          		Remove
		          	</Button>
		          </td>
		      </tr>			
			);

		})

		setTableRows(cartItems);

		let tempTotal = 0;

		cart.forEach((item)=> {
			tempTotal += item.subtotal;
		});

		setTotal(tempTotal);

	}, [cart, checkCartNo]);

	const checkout = () => {

		const checkoutCart =  cart.map((item) => {
			return {
				productId:item.productId,
				productName: item.name,
				quantity: item.quantity,
				price: item.price,
			}
		})

		fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				products: checkoutCart,
				totalAmount: total
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {
				alert("Order placed! Thank you!");
				localStorage.removeItem('cart');
				setWillRedirect(true);
				checkCartNo(cart);
				
				
			} else {
				alert("Something went wrong. Order was NOT placed.");
			}

		})
	}
	
	return(
		willRedirect === true ? 
			<Navigate to={{pathname:"/orders", state: {from:"specific"}}}/>

		:
			cart.length <= 0 ? 
					<div>
						<h3 className="text-center mt-5 pt-5">
							Your cart is empty! <Link to="/products">Start shopping.</Link>
						</h3>
					</div>
				:
				<Container>
					<h2 className="text-center my-4">Your Shopping Cart</h2>
					<Table striped bordered hover responsive>
						<thead className="bg-dark text-white">
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{tableRows}
							<tr>
								<td colSpan="3">
									<Button 
										variant="success"
										block = "true"
										onClick={()=> checkout()}
									>
										Checkout
									</Button>
								</td>
								<td colSpan="2">
									<h3>Total: ₱{total}</h3>
								</td>
							</tr>
						</tbody>						
					</Table>
				</Container>
	);
	
}
 
export default MyCart;