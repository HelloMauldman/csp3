import React, { useState, useEffect, useContext } from 'react';
import AdminView from '../components/AdminView';
import CustomerView from '../components/CustomerView';
import { Container, Button, Form, InputGroup, CloseButton } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
 
export default function Products(){
 
   const { user } = useContext(UserContext)
   const [ products, setProducts ] = useState([]);
   const [keyword, setKeyword] = useState("");

   const fetchData = () => {
       fetch(`${process.env.REACT_APP_API_URL}/products/all`)
       .then(res => res.json())
       .then(data => {
           setProducts(data);
       })
   };

   const filterSearch = (e) =>{
    console.log(products)
        e.preventDefault();
        const newFilter = products.filter((product)=>{
            return product.name.toLowerCase().includes(keyword.toLowerCase())
        })
        if(newFilter.length <= 0){
            alert("There are no such item/s.")
            return(
                <Navigate to={"/products"}/>
                )
        }
        setProducts(newFilter)
        console.log("the function is working.")
   }

   useEffect(() => {
       fetchData();
   }, []);
 
   return(
       <Container>
            <Form onSubmit={(e)=>{filterSearch(e)}}>
                <Form.Group className="mt-4 rounded-3" >
                    <InputGroup>
                        <Form.Control placeholder="Search Items" aria-label="search item" aria-describedby="basic-addon2" onChange={e => setKeyword(e.target.value)} />
                        <CloseButton className="mt-2" onClick={()=>{window.location.reload(false)}}/>
                        <Button variant="primary rounded-3" id="button-addon2" type="submit">Search</Button>
                    </InputGroup>
                </Form.Group>
            </Form>
           {
        
               user.isAdmin === true ?
                   <AdminView productsData={products} fetchData={fetchData}/>
               :
                   <CustomerView productsData={products}/>
           }
           
       </Container>
   )
}
 