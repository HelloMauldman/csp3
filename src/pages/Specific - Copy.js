import {Container}  from "react-bootstrap";
import UserContext from '../UserContext';
import React, { useState, useEffect, useContext} from 'react';
import {useParams} from "react-router-dom";
import Product from '../components/Product';


const Specific = ()=>{
	const[products, setProducts] = useState([]);

	const{user} = useContext(UserContext);

	const {productId} = useParams();
	// console.log(productId)

	const fetchData = () => {
       fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
       .then(res => res.json())
       .then(data => {
       	// console.log(data)
           setProducts(data);
       })
   };
   useEffect(() => {
       fetchData();
   }, []);
// 

	return(
		<Container>
			<h1>Product</h1>
			<Product data={products}/>
		</Container>
	);
}

export default Specific;