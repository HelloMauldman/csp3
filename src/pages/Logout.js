import {useContext,useEffect} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
 
export default function Logout(){
 
   //destructure our UserContext and get our setUser and unsetUser functions from our context.
   const {setUser,unsetUser} = useContext(UserContext);
 
   //clear the localStorage
   unsetUser();
 
   //add a useEffect to run our setUser. This useEffect will have an empty dependency array.
   useEffect(()=>{
 
       //update our global user state to its initial values:
       setUser({
 
           id: null,
           isAdmin: null,
           email: null
 
       })

       alert("Logout Successfully!")
 
 
   },[setUser])
 
   return (
 
           <Navigate to={{pathname: '/login', state: { from: 'logout'}}} />
 
       )
 
}
